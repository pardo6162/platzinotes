## Listening
El objetivo es comprender lo que se escucha en inglés en general o en detalle
### ¿ Por qué es difícil ?
- Sonidos distintos dependiendo el acento
- No se habla como se escribe
- Velocidad 
- Vocabularío
- Linking (Juntan palabras para hablar más rápido)

### Recursos
- Películas y Series
- Podcast
- Música
- Noticias
- Meetups
- Clubes de conversación

### Recomendaciones
- TED Talks
- Lyrics training

## Reading
Comprender un texto escrito
COmpetencia pasiva

### Retos
- Vocabulario (No hay gestos ni contextos)
- Reglase gramáticales
- No tenemos compresión de lectura en español

### Ejercicios
- Cambiar todos lod sipositivos a inglés
- Leer libors, articulos, noticias, ...etc
- Crear listas de vocabulario
- Hacer resumenes de lo que quedo de algo que leimos

### Recursos
- News in levels
- 365 ensayos para presonas que aprenden ingles

## Speaking 
Producir un mensaje claro y coherente
Competencia activa

### Retos
- Vocabulario y gramática
- Sonidos diferentes en español e ingles
- Inseguridad
- Traducción mental

### Ejercicios
- Practicar listening
- Estudiar Vocabulario y Gramática
- Pensar en inglés
- Hablar solo frente a un espejo
- Asistir a conferencias y meetups

### Recursos
- Tandem
- HelloTals
- Duolingo
- Google Traslate (decirle una palabra a ver si la reconoce)
- Imitar 


## Writing
Producir un texto coherente
Competencia activa

### Retos
- Pereza
- Requiere más esfuerzo

### Ejercicios 
- Crear situaciones hipoteticas (Redactarlas)
- Escribir articulos
- Blogs
- Tomar notas en ingles
- Practicar reading

### Recursos
- Language Tool
- Grammarly



