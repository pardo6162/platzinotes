## Patrones de diseño en Django

## MVC ( Model, View, Controler)

![Image of MVC](images/MVC.png)

En Django se usa el MTV (Model, Template, View)

**Model**: Estructura de los datos.
**Template**: Vista que muestra los datos
**View**: A que template se le asignan los datos
***Patrón de diseño***: *Solución reutilizable en problemas comunes*


