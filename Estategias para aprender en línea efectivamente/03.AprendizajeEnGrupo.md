### Aprendizaje en grupo
- Ayuda a mantener la motivación
- Compartir dudas
- Poder explicar algo

> " Entre más se explica, más se aprende "

## Tecnicas:
**Ser profesor por un día**
    - Obliga a repasar un tema
    - Hay un nivel de reflexión más grupal
    - Se pueden tomar más notas

> # Tecnica Feyman 
> 1. Elegir un tema
> 2. Escribirlo en palabras propias, luego leerlo en voz alta
> 3. Arreglar lo que no está bien explicado
> 4. Repasar y simplificar más
 
## ¿ Como aprender a evaluarse ?
- Buscar que otra diseñe la prueba
- Buscar poner a prueba lo aprendido dentro de diferentes situación

