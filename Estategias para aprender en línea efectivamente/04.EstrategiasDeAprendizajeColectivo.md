### Diálogo
- Para poder tener un diálogo se tiene que crear una postura
- Formar argumentos
- La idea es convencer a las demás personas
- Realizar un aporte a lo que se aprende
 
**¡ BONUS !**
**Metacognición**:
Reflexionar - Analizar - Criticar
 
**Pasos para usar Platzi**
1. Trazar un objetivo
2. Visualizar el objetivo mientras se aprende
3. Evaluar al finalizar el objetivo
- Crear un plan de cursos
- Completar todos los cursos
- Evitar distracciones
- Calendarizar las clases
- Repasar o aprender todos los días
- Darse recompensas
